﻿// Run when the DOM is complete
$(document).ready(function(){
	// Add the stylesheed newsStyle.css in the head of the document
	// No need for the user of the script to remember this, we can do it here
	$("head").append( '<link rel="stylesheet" href="newsStyle.css"/>' );
});	

/**
 * A function can also act as a class definition and constructor in one
 * var v = new News('something') will create a new object v with the contents 
 * of the News function
 *
 * var id is the id of the div tag to convert into a newsticker
 */
function News(id) {
	// add the variables to the object
	this.items = Array ();
	this.current = 0;
	this.id = id;
	// Add the div tags to the newsticker div tag
	$(id).append ('<div style="z-index: 2"></div>');
	$(id).append ('<div style="z-index: 1"></div>');
	// run the fetchNews method of this object
	// Defined below
	this.fetchNews();
}

/**
 * News is a class definition, now add a function fetchNews to this class.
 * The prototype keyword is one of the ways this can be done.
 * This function does the same as the previous fetchNews function.
 */
News.prototype.fetchNews = function() {
	// Variable scope in JavaScript is a funny thing, but this has a defined meaning
	// This refers to the current object, save this (meaning this newsticker object 
	// in the variable "me"
	var me = this;
	// Reset the items array since we reload the array each time we get to the end
	this.items = Array ();
	// For the same reason we reset the pointer variable
	this.current = 0;

	// Use an ajax call to get the newsfeed
	$.ajax({
		url: 'fetchFileIso.php',		// Use a proxy script
		data: {'url':'http://www.itavisen.no/rss.xml'},
		dataType: 'xml',						// Use XML as container for the received data
		success: function (data) {	// This function gets called when the data from the server is ready
			// All news items is contained within an item tag
			var tmp = data.getElementsByTagName("item")
			// Loop through the array
			for (i=0; i<tmp.length; i++) {
				var item = new Object();		// Creates a new empty object, handy for storing properties
				// Store the title, description and link of the news item
				item.title = tmp[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
				item.descr = tmp[i].getElementsByTagName("description")[0].childNodes[0].nodeValue;
				item.link = tmp[i].getElementsByTagName("link")[0].childNodes[0].nodeValue;
				// Add the news item to the array (at the next available position)
				// Now we need the "me" variable defined above, since "this" here would refer to the 
				// object performing the ajax call
				// "me" is still the reference to the newsticker object
				me.items[me.items.length] = item;
			}
			// Call the showNews method of the newsticker object (method defined below)
			me.showNews();
		}
	});
}

/**
 * Add the method showNews to the class News.
 * This method stilll does the same as it did in the previous examples.
 */
News.prototype.showNews = function() {
	// Inside the #newsstand div tag there is two more div tags placed one on top of the other
	// The first div tag is placed on top of the last, but they are pixel for pixel placed on top 
	// of each other
	// This means that we can place the old content on the last div (the one in the back)
	// and place the new content on the first div and fade in the new content on top of the old
	
	// Start off by placing the previous displayed content on the back div
	// (Copy contents from the first to the last div

	// Note $(this) refers to this jQuery object, whie
	$(this.id+" > div").last().html ($(this.id+" > div").first().html());
	// Then we hide the first (front) div tag
	$(this.id+" > div").first().hide();
	// Now place new content on the first (front) div tag
	$(this.id+" > div").first().html ("<h2>"+this.items[this.current].title+"<h2>");
	$(this.id+" > div").first().append ("<a href='"+this.items[this.current].link+"'>"+this.items[this.current].descr+"</a>");
	// Lastly we fade the first (front) div tag in over the last (back) div
	$(this.id+" > div").first().fadeIn("slow");
	// Advance the current pointer to the next item
	this.current++;
	// If we have moved past the length of the array
	if (this.current>this.items.length-1)
			// Update the array containing the news items, then display again
		setTimeout (this.id.substring(1)+".fetchNews()", 2500);
	else
		// Display the next news item in two and a half seconds time
		setTimeout (this.id.substring(1)+".showNews()", 2500);
}