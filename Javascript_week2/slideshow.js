﻿$(document).ready(function(){
	$("head").append( '<link rel="stylesheet" href="slideshowStyle.css"/>' );
});	

function Slideshow(id, subject) {;
	this.items = Array ();
	this.current = 0;
	this.id = id;
	this.subject = subject;
	$(id).append ('<div style="z-index: 2; position: absolute"></div>');
	$(id).append ('<div style="z-index: 1; position: absolute"></div>');
	this.fetchImages();
}

Slideshow.prototype.fetchImages = function() {
	this.items = Array ();
	this.current = 0;
	var me = this;
	$.ajax({
		url: 'fetchFile.php',
		data: {'url':'http://picasaweb.google.com/data/feed/base/all?alt=rss&kind=photo&access=public&filter=1&q='+me.subject+'&hl=en_US'},
		success: function (data) {
			var tmp = data.getElementsByTagName("item")
			for (i=0; i<tmp.length; i++) {
				var item = new Object();
				item.url = tmp[i].getElementsByTagName("media:thumbnail")[1].attributes.getNamedItem("url").value;
				item.height = tmp[i].getElementsByTagName("media:thumbnail")[1].attributes.getNamedItem("height").value;
				item.width = tmp[i].getElementsByTagName("media:thumbnail")[1].attributes.getNamedItem("width").value;
				me.items[me.items.length] = item;
			}
			$(me.id+" > div").first().hide();
			me.showSlides();
		}
	});
}

Slideshow.prototype.showSlides = function() {
	$(this.id+" > div").last().fadeOut ("slow");
	$(this.id+" > div").first().fadeIn ("slow", function () {
		$(this.id+" > div").last().html ($("#images > div").first().html());
		$(this.id+" > div").last().show ();
		$(this.id+" > div").first().hide ();
		$(this.id+" > div").first().html (" &nbsp;");
		$(this.id+" > div").first().append ("<img src='"+this.items[this.current].url+"'>");
		$(this.id+" > div > img").first().css ("position", "absolute");
		$(this.id+" > div > img").first().css ("top", Math.round((150-this.items[this.current].height)/2)+"px");
		$(this.id+" > div > img").first().css ("left", Math.round((150-this.items[this.current].width)/2)+"px");
	});
	this.current++;
	if (this.current>this.items.length-1)
		this.current = 0;
	setTimeout ("slideshow.showSlides()", 3500);
}
