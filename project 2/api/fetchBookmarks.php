<?php 
header ("Content-type: application/json");

require_once '../include/db.php';

/** TODO: Should also consider user/public status **/

$sql = 'SELECT id, url, title, description, OCTET_LENGTH(thumbnail) AS thumbnailSize FROM bookmarks where categoryid=?';
$sth = $db->prepare($sql);
$sth->execute (array ($_GET['id']));	// Run the query
echo json_encode ($sth->fetchAll(PDO::FETCH_ASSOC));	// Get all data from the database