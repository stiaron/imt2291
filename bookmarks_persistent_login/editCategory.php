<?php
session_start();					// Start the session

require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff
require_once 'classes/categories.php';

$pageTitle = "Edit category";
require_once 'include/heading.php';
?>

<div class="container">
<?php 
if ($user->isLoggedIn()) {
	echo '<div class="row"><div class="col-sm-4 col-xs-12">';
	echo '<div class="panel panel-default"><div class="panel-heading">Categories</div>';
	echo '<div class="panel-body">';
	$categories->insertCategoriesTree();	// Insert categories tree
	echo '</div></div>';
	echo '</div><div class="col-sm-8 col-xs-12">';
	echo '<div class="panel panel-default"><div class="panel-heading">Edit category</div>';
	echo '<div class="panel-body">';
	$categories->insertEditCategoryForm();	// Insert edit category form
	echo '</div></div>';
	echo '</div>';
	
}
?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="BootstrapTreeNav/dist/js/bootstrap-treenav.min.js"></script>
</body>
</html>