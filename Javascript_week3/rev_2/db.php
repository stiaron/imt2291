<?php
try {
	// Connect to database blogg2 on localhost with the given credentials
	$db = new PDO('mysql:host=127.0.0.1;dbname=blogg2', 'imt2291', 'imt2291');
} catch (PDOException $e) {
	// Do not continue if we can't log in
	die ('Kunne ikke koble til serveren : ' . $e->getMessage());
}