﻿<?php
/**
 * Used by changeUserDetails in blogg.js to get data to fill into the dialog
 * for changing user details.
 */
 
// This makes jQuery interpret returned data as json as default
header ('Content-type: application/json');

// Start the session handling system
session_start();
// Set up the database connection
require_once 'db.php';

// SQL statement to find information about a user
$sql = 'SELECT uid, givenname, surename, url, pwd FROM users WHERE uid=?';
$sth = $db->prepare ($sql);
// Send the session variable user as a parameter
// No active session means no row will be found
$sth->execute (array ($_SESSION['user']));

if ($row=$sth->fetch())	{		// We found the user
	echo json_encode ($row);	// Return all data about the user
} else							// No user found, return an error message
	echo json_encode (array ('error'=>'Ikke logget på'));
?>
	