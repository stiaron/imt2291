﻿<?php
/**
 * Called from the newUser function in blogg.js to create a new user
 */

// This makes jQuery interpret returned data as json as default
header ('Content-type: application/json');
 
// Set up the database connection
require_once 'db.php';
// SQL statement to insert a new user into the database, note that the uid field of the
// database is declared as UNIQUE, meaning no two users can have the same username
$sql = 'INSERT INTO users (uid, givenname, surename, url, pwd) VALUES (?, ?, ?, ?, ?)';
$sth = $db->prepare ($sql);
// Add the user to the database
$res = $sth->execute (array ($_POST['uname'], $_POST['givenname'], $_POST['surename'], $_POST['url'], md5($_POST['pwd'])));
if ($res==1)		// If a single row was inserted a new user was created
	echo json_encode (array ('ok'=>'OK'));
else				// If no row was inserted it's because of a duplicate username
	echo json_encode (array ('message'=>'Brukernavnet finnes allerede i databasen.'));
?>