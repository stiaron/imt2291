<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
</head>
<body>
<form method="post" action="xpathExplorer.php">
URL: <input type="url" name="url"/></br>
XPath: <input type="text" name="xpath"/></br>
<input type="submit" value="try"/></form>

<?php
if (!isset($_POST['url']))
	die();
$content = file_get_contents($_POST['url']);
$doc = new DOMDocument();
$encoding = mb_detect_encoding($content);
$converted = mb_convert_encoding($content, "HTML-ENTITIES", $encoding);
$doc->loadHTML($converted);

$xpath = new DOMXpath($doc);
$elements = $xpath->query($_POST['xpath']);

if (!is_null($elements)) {
	foreach ($elements as $element) {
		echo '<p><b>'.$element->tagName.'</b> (';
		for ($i=0; $i<$element->attributes->length; $i++) {
			$attr = $element->attributes->item($i);
			echo "{$attr->name}: {$attr->value}\t";
		}
		echo ")";
		if ($element->nodeValue!="")
			echo " => ".$element->nodeValue;
		echo "</p>\n";
	}
}