<?php
/*
 * Show all link items from the head with rel=alternate and 
 * type=application/rss+xml as links to a new page sending the href
 * as a parameter and using the title as the link text
 */
$doc = new DOMDocument();
$doc->loadHTMLFile("http://www.vg.no");

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/link");

if (!is_null($elements)) {
	foreach ($elements as $element) {
		$rel = "";
		$type = "";
		$title = "";
		for ($i=0; $i<$element->attributes->length; $i++) {
			$attr = $element->attributes->item($i);
			if ($attr->name=="rel")
				$rel = $attr->value;
			else if ($attr->name=="type")
				$type = $attr->value;
			else if ($attr->name=="title")
				$title = $attr->value;
			else if ($attr->name=="href"&&$rel=="alternate"&&$type=="application/rss+xml")
				echo "<a href='example8.php?page=".urlencode($attr->value)."'>$title</a></br>";	
		}
	}
}