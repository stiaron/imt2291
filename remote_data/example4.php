<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
</head>
<body>
<?php
/*
 * DOM parsing of HTML doesn't like UTF-8 encoded characters
 * Find the title of the document
 */
$content = file_get_contents("http://www.hig.no");
$doc = new DOMDocument();
$encoding = mb_detect_encoding($content);
$converted = mb_convert_encoding($content, "HTML-ENTITIES", $encoding);
$doc->loadHTML($converted);

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/meta");

if (!is_null($elements)) {
	foreach ($elements as $element) {
		for ($i=0; $i<$element->attributes->length; $i++) {
			$attr = $element->attributes->item($i);
			echo "{$attr->name}: {$attr->value}\t";	
		}
		echo "</br>\n";
	}
}

$xpath = new DOMXpath($doc);
$elements = $xpath->query("/html/head/title");

if (!is_null($elements)) {
	foreach ($elements as $element) {
		echo "Title: ".$element->nodeValue;
	}
}

