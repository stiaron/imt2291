﻿<?php
require_once 'db.php';
$sql = 'SELECT SQRT( POW( ( :lat - lat ) , 2 ) + POW( ( :long - lng ) , 2 ) ) AS distance, 
               id, title, round(lat,2) as lat, round(lng,2) as lng, DATE_FORMAT(`when`, "%a %e/%c-%Y (%k:%i)") as `date`, givenname, surename
        FROM entry, users
				WHERE entry.uid=users.uid
        ORDER BY distance, `when` desc
        LIMIT 20';
$sth = $db->prepare ($sql);
$sth->bindParam (':lat', $_POST['lat']);
$sth->bindParam (':long', $_POST['long']);
$sth->execute ();
$data = array();
while ($row = $sth->fetch()) {
	if ($row['lat']==0&&$row['lng']==0)
		continue;
	$data[] = array ('lat'=>$row['lat'], 'lng'=>$row['lng']);
	$data[0]['innlegg'][] =  "<li><a href='javascript:showEntryDialog({$row['id']});'>{$row['title']}</a> av {$row['givenname']} {$row['surename']} ({$row['date']})</li>";
	break;
}	
while ($row = $sth->fetch()) {
	if ($row['lat']==0&&$row['lng']==0)
		continue;
	if ($row['lat']==$data[count($data)-1]['lat']&&$row['lng']==$data[count($data)-1]['lng'])
		$data[count($data)-1]['innlegg'][] = "<li><a href='javascript:showEntryDialog({$row['id']});'>{$row['title']}</a> av {$row['givenname']} {$row['surename']} ({$row['date']})</li>";
	else {
		$data[] = array ('lat'=>$row['lat'], 'lng'=>$row['lng']);
		$data[count($data)-1]['innlegg'][] = "<li><a href='javascript:showEntryDialog({$row['id']});'>{$row['title']}</a> av {$row['givenname']} {$row['surename']} ({$row['date']})</li>";
	}
}
echo json_encode ($data);
?>