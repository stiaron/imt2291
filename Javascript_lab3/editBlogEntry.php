﻿<?php 

// We will return json data
header ("Content-type: application/json");
session_start ();
require_once ('db.php');
if (!isset($_SESSION['user']))	// No go if not logged in
	die ("You must be logged in");
$sql = 'SELECT title, entry, lat, lng FROM entry WHERE uid=? AND id=?';
$sth = $db->prepare ($sql);
$sth->execute (array ($_SESSION['user'], $_GET['id']));
if ($row=$sth->fetch(PDO::FETCH_ASSOC)) {	// Get information form database
	$title = $row['title'];
	$entry = $row['entry'];
	$lat = $row['lat'];
	$lng = $row['lng'];
} else	// No such entry
	die ("Could not find blog entry");

// Return the form, ready to go
$data['html'] = "<form onsubmit='javascript:return false'>
<input type='hidden' name='id' value='{$_GET['id']}'/>
<input type='hidden' name='lat'/><input type='hidden' name='lng'/>
<label for='title'>Tittel på blogg innlegget</label>
<input type='text' name='title' value='$title'><br/>
<textarea id='editBlogEntry' style='width:100%; height:400px'>$entry</textarea>
<input type='button' onclick='javascript:storeChangedBlogEntry();' value='Lagre blogg innlegget'/>
</form>";

// Return lat/lng to show the map
$data['lat'] = $lat;
$data['lng'] = $lng;

echo json_encode ($data);