﻿<?php
header ("Content-type: application/json");
session_start();
require_once 'db.php';

if (!isset($_SESSION['user']))
	die (json_encode (array ('error'=>'No user logged on')));

$sql = 'DELETE FROM files where id=? and uid=?';
$sth = $db->prepare ($sql);
$sth->execute (array($_POST['id'], $_SESSION['user']));
?>