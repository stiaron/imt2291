﻿$(document).ready (function () {
	$('head').append ('<link rel="stylesheet" href="folders.css"/>');
	$('#fileUpload').dialog({autoOpen:false, width: "500px", modal: true });
});

function Folders () {
  this.current = -1;
	this.folderSelected = null;
	this.fileClick = null;
}

var folders = new Folders ();

Folders.prototype.init = function () {
	$.ajax ({
		url: 'fetchFolders.php',
		data: {'id': -1},
		type: 'post',
		success: function (data) {
			$('#folders').html ('<ul class="folders"></ul>');
			for (var i=0; i<data.length; i++) {
				$('#folders .folders').append ('<li id="folder_'+data[i].id+'"><nobr><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></nobr></li>');
				$('#folders .folders').last().loaded = false;
			}
		}
	});
}

Folders.prototype.openClose = function (id) {
	if (this.current>0)
		$('#folder_'+this.current).toggleClass ('selected');
	this.current = id;
		$('#folder_'+this.current).toggleClass ('selected');
	if ($('#folder_'+id)[0].loaded) {
		$('#folder_'+id+' ul').toggle ();
		if ($('#folder_'+id)[0].hasSubFolders) {
			$('#folder_'+id).toggleClass ('opened');
			$('#folder_'+id).toggleClass ('closed');
		}
	} else {
		$('#folder_'+id)[0].loaded = true;
		$('#folder_'+id).append ('<ul class="folders"></ul>');
		$.ajax ({
			url: 'fetchFolders.php',
			data: {'id': id},
			type: 'post',
			success: function (data) {
				if (data.length>0) {
					$('#folder_'+id)[0].hasSubFolders = true;
					$('#folder_'+id).toggleClass ('opened');
				}	else
					$('#folder_'+id)[0].hasSubFolders = false;
				for (var i=0; i<data.length; i++) {
					$('#folder_'+id+' ul').append ('<li id="folder_'+data[i].id+'"><nobr><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></nobr></li>');
					$('#folder_'+id+' ul li').last().loaded = false;
				}
			}
		});
	}
	if (this.folderSelected!=null)
		this.folderSelected (id);
}

Folders.prototype.createNewFolder = function(name) {
	if (this.current==-1)
		return;
	$.ajax ({
		url: 'createNewFolder.php',
		data: {'name': name, 'parentId': folders.current},
		type: 'post',
		success: function (data) {
			if (data.error) {
				alert (data.error);
				return;
			}
			var newSelected = 0;
			$('#folder_'+folders.current).toggleClass ('selected');
			$('#folder_'+folders.current+' ul').empty ();
			$('#folder_'+folders.current+' ul').show ();
			$('#folder_'+folders.current).hasSubFolders = true;
			if (!$('#folder_'+folders.current).hasClass ('opened'))
				$('#folder_'+folders.current).toggleClass ('opened');
			for (var i=0; i<data.length; i++) {
				$('#folder_'+folders.current+' ul').append ('<li id="folder_'+data[i].id+'"><nobr><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></nobr></li>');
				$('#folder_'+folders.current+' ul li').last().loaded = false;
				if (data[i].name==name)
					newSelected = data[i].id;
			}
			folders.current = newSelected;
			$('#folder_'+folders.current).toggleClass ('selected');
		}
	});
}

Folders.prototype.removeFolder = function() {
	if (this.current==-1)
		return;
	$.ajax ({
		url: 'removeFolder.php',
		data: {'id': folders.current},
		type: 'post',
		success: function (data) {
			if (data.error) {
				alert (data.error);
				return;
			}
			folders.current = data[0].id; 
			$('#folder_'+folders.current+' ul').empty ();
			$('#folder_'+folders.current).hasSubFolders = false;
			if ($('#folder_'+folders.current).hasClass ('opened'))
				$('#folder_'+folders.current).toggleClass ('opened');
			if ($('#folder_'+folders.current).hasClass ('closed'))
				$('#folder_'+folders.current).toggleClass ('closed');
			if (data.length>1)
				$('#folder_'+folders.current).toggleClass ('opened');
			for (var i=1; i<data.length; i++) {
				$('#folder_'+folders.current+' ul').append ('<li id="folder_'+data[i].id+'"><nobr><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></nobr></li>');
				$('#folder_'+folders.current+' ul li').last().loaded = false;
				$('#folder_'+folders.current).hasSubFolders = true;
			}
			if ($('#folder_'+folders.current).hasSubFolders)
				$('#folder_'+folders.current).toggleClass ('opened');
			$('#folder_'+folders.current).toggleClass ('selected');
		}
	});
}

Folders.prototype.renameFolder = function(name) {
	if (this.current==-1)
		return;
	$.ajax ({
		url: 'renameFolder.php',
		data: {'name': name, 'id': folders.current},
		type: 'post',
		success: function (data) {
			if (data.error) {
				alert (data.error);
				return;
			}
			var renamedFolder = folders.current;
			folders.current = data[0].id; 
			$('#folder_'+folders.current+' ul').empty ();
			for (var i=1; i<data.length; i++) {
				$('#folder_'+folders.current+' ul').append ('<li id="folder_'+data[i].id+'"><nobr><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></nobr></li>');
				$('#folder_'+folders.current+' ul li').last().loaded = false;
			}
			$('#folder_'+folders.current).toggleClass ('selected');
			folders.openClose(renamedFolder);
		}
	});
}


Folders.prototype.showFiles = function () {
	if (this.current==-1)
		return;
	$.ajax ({
		url: 'fetchFiles.php',
		data: {'id': folders.current},
		type: 'post',
		success: function (data) {
			if (data.error) {
				alert (data.error);
				return;
			}
			$('#files .filelist').empty ();
			for (var i=0; i<data.length; i++) {
				var tmp = '<input type="checkbox" value="'+data[i].id+'" title="Klikk her og velg så endre navn/beskrivelse eller slette filen fra Filer menyen"/><a href="javascript:folders.fileClick('+data[i].id+');" title="'+data[i].description+'"><span class="name"><nobr>'+data[i].name+'</nobr></span><span class="date"><nobr>'+data[i].date+'</nobr></span><span class="type"><nobr>'+data[i].mime+'</nobr></span><span class="size"></nobr>'+data[i].size+'</nobr></span></a>';
				$('#files .filelist').append (tmp);
			}
		}
	});
}

Folders.prototype.renameFile = function (id, name) {
	$.ajax ({
		url: 'renameFile.php',
		data: {'id': id, 'name': name},
		type: 'post',
		success: function (tmp) {
			folders.showFiles();
		}
	});
}

Folders.prototype.changeFileDescr = function (id, descr) {
	$.ajax ({
		url: 'changeFileDescr.php',
		data: {'id': id, 'descr': descr},
		type: 'post',
		success: function (tmp) {
			folders.showFiles();
		}
	});
}

Folders.prototype.removeFile = function (id) {
	$.ajax ({
		url: 'removeFile.php',
		data: {'id': id},
		type: 'post',
		success: function (tmp) {
			folders.showFiles();
		}
	});
}