﻿$(document).ready (function () {
	$('head').append ('<link rel="stylesheet" href="folders.css"/>');
});

function Folders () {
  var current = -1;
	var folderSelected = null;
}

var folders = new Folders ();

Folders.prototype.init = function () {
	$.ajax ({
		url: 'fetchFolders.php',
		data: {'id': -1},
		type: 'post',
		success: function (data) {
			$('#folders').html ('<ul class="folders"></ul>');
			for (var i=0; i<data.length; i++) {
				$('#folders .folders').append ('<li id="folder_'+data[i].id+'"><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></li>');
				$('#folders .folders').last().loaded = false;
			}
		}
	});
}

Folders.prototype.openClose = function (id) {
	if (this.current>0)
		$('#folder_'+this.current).toggleClass ('selected');
	this.current = id;
		$('#folder_'+this.current).toggleClass ('selected');
	if ($('#folder_'+id)[0].loaded) {
		$('#folder_'+id+' ul').toggle ();
		if ($('#folder_'+id)[0].hasSubFolders) {
			$('#folder_'+id).toggleClass ('opened');
			$('#folder_'+id).toggleClass ('closed');
		}
	} else {
		$('#folder_'+id)[0].loaded = true;
		$('#folder_'+id).append ('<ul class="folders"></ul>');
		$.ajax ({
			url: 'fetchFolders.php',
			data: {'id': id},
			type: 'post',
			success: function (data) {
				if (data.length>0) {
					$('#folder_'+id)[0].hasSubFolders = true;
					$('#folder_'+id).toggleClass ('opened');
				}	else
					$('#folder_'+id)[0].hasSubFolders = false;
				for (var i=0; i<data.length; i++) {
					$('#folder_'+id+' ul').append ('<li id="folder_'+data[i].id+'"><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></li>');
					$('#folder_'+id+' ul li').last().loaded = false;
				}
			}
		});
	}
}

Folders.prototype.createNewFolder = function(name) {
	if (this.current==-1)
		return;
	$.ajax ({
		url: 'createNewFolder.php',
		data: {'name': name, 'parentId': folders.current},
		type: 'post',
		success: function (data) {
			if (data.error) {
				alert (data.error);
				return;
			}
			var newSelected = 0;
			$('#folder_'+folders.current).toggleClass ('selected');
			$('#folder_'+folders.current+' ul').empty ();
			$('#folder_'+folders.current).hasSubFolders = true;
			if (!$('#folder_'+folders.current).hasClass ('opened'))
				$('#folder_'+folders.current).toggleClass ('opened');
			for (var i=0; i<data.length; i++) {
				$('#folder_'+folders.current+' ul').append ('<li id="folder_'+data[i].id+'"><span class="openClosed">&nbsp;</span><span class="foldericon">&nbsp;</span><a href="javascript:folders.openClose('+data[i].id+');">'+data[i].name+'</a></li>');
				$('#folder_'+folders.current+' ul li').last().loaded = false;
				if (data[i].name==name)
					newSelected = data[i].id;
			}
			folders.current = newSelected;
			$('#folder_'+folders.current).toggleClass ('selected');
		}
	});
}