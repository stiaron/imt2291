﻿-- phpMyAdmin SQL Dump
-- version 3.3.5
-- http://www.phpmyadmin.net
--
-- Vert: 127.0.0.1
-- Generert den: 23. Mar, 2011 10:10 AM
-- Tjenerversjon: 5.1.49
-- PHP-Versjon: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `file_library`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `folderid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `public` enum('y','n') NOT NULL,
  `name` varchar(128) NOT NULL,
  `mime` varchar(128) NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` longblob NOT NULL,
  `size` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dataark for tabell `files`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `folders`
--

CREATE TABLE IF NOT EXISTS `folders` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dataark for tabell `folders`
--

INSERT INTO `folders` (`id`, `parentid`, `uid`, `name`) VALUES
(1, -1, 1, 'Bilder'),
(2, -1, 1, 'Video'),
(3, -1, 1, 'Dokumenter'),
(4, 1, 1, 'Sommerbilder'),
(5, 1, 1, 'Vinterbilder'),
(13, 1, 1, 'VÃ¥rbilder'),
(7, 1, 1, 'HÃ¸stbilder'),
(12, 11, 1, '2010'),
(20, 13, 1, '2011'),
(25, -1, 1, 'Meg');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(128) NOT NULL,
  `pwd` char(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dataark for tabell `users`
--

INSERT INTO `users` (`id`, `uid`, `pwd`) VALUES
(1, 'okolloen@hig.no', md5('tullball'));
