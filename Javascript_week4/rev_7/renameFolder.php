﻿<?php
header ("Content-type: application/json");
session_start();
require_once 'db.php';

if (!isset($_SESSION['user']))
	die (json_encode (array ('error'=>'No user logged on')));

$sql = 'SELECT * FROM folders WHERE uid=? and id=?';
$sth = $db->prepare ($sql);
$sth->execute (array ($_SESSION['user'], $_POST['id']));
if (!($row = $sth->fetch()))
	die (json_encode (array ('error'=>'Error during database operation')));
$parentid = $row['parentid'];
$sth->fetchAll();

$data[] = array ('id'=>$parentid, 'name'=>'parent');

$sql = 'UPDATE folders SET name=? WHERE uid=? AND id=?';
$sth = $db->prepare ($sql);
$res = $sth->execute (array ($_POST['name'], $_SESSION['user'], $_POST['id']));
if ($res==0)
	die (json_encode (array ('error'=>'Error during database operation')));
	
// Return folders for user
$sql = 'SELECT name, id, uid FROM folders WHERE uid=? and parentid=? order by name';
$sth = $db->prepare ($sql);
$sth->execute (array ($_SESSION['user'], $parentid));
die (json_encode  (array_merge($data, $sth->fetchAll ())));
?>