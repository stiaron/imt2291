<?php
header ("Content-type: text/html");
session_start ();
	require_once 'db.php';
	
	if (!isset($_SESSION['user'])) {
		die ('<script type="text\javascript">\nalert ("Du er ikke logget på!!!");\n</script>');
	}
	
	$sql = 'INSERT INTO files (folderid, uid, public, name, mime, description, content, size, date)
	        VALUES (?, ?, ?, ?, ?, ?, ?, ?, now())';
	if (isset($_POST['public']))
		$public = 'y';
	else
		$public = 'n';
	if (is_uploaded_file($_FILES['file']['tmp_name'])) {
		$content = file_get_contents($_FILES['file']['tmp_name']);
		$name = $_FILES['file']['name'];
		$mime = $_FILES['file']['type'];
		$size = $_FILES['file']['size'];
		$sth = $db->prepare ($sql);
		$sth->execute (array ($_POST['folderid'], $_SESSION['user'], $public, $name, $mime, $_POST['descr'], $content, $size));
		//print_r ($sth->errorInfo());
	} else
		die ('<script type="text\javascript">\nalert ("Ingen fil lastet opp!!!");\n</script>');
?><script type="text/javascript">
window.top.window.fileUploaded();
window.location = 'db.php';
</script>