﻿<?php
/**
 * Create a database connection, all database information is stored here
 */
try {
	$db = new PDO('mysql:host=127.0.0.1;dbname=file_library', 'root', 'imt2291');
} catch (PDOException $e) {	// If no database connection then we can not continue
    die ('Kunne ikke koble til serveren : ' . $e->getMessage());
}
?>