<?php
/**
 * Script used by the DnD upload feature, gets the file and store it in the database.
 * For files uploaded by DnD we assume no description and always public files.
 */
session_start ();
require_once 'db.php';
	
if (!isset($_SESSION['user'])) {	// Must be logged in to perform file write operations
	die ('<script type="text\javascript">\nalert ("Du er ikke logget p�!!!");\n</script>');
}

$raw_post = file_get_contents("php://input");	// Get the content of the file
$headers = apache_request_headers();			// Filename, mime type and folderid is found in the header information

// SQL query to insert the new file into the database
$sql = 'INSERT INTO files (folderid, uid, public, name, mime, description, content, size, date)
		VALUES (?, ?, "y", ?, ?, "", ?, ?, now())';
$sth = $db->prepare ($sql);
// Send the file and other info to the database
$sth->execute (array ($headers['folder'], $_SESSION['user'], $headers['filename'], $headers['type'], $raw_post, $headers['size']));

// Nothing is ever returned as all return values will be ignored anyway
?>