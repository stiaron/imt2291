<?php
/**
 * Returns a file from the database as it where a file in the filesystem.
 */
session_start ();
if (isset ($_SESSION['user']))
	$user = $_SESSION['user'];
else
	$user = '';
require_once 'db.php';

// Return public files or files belonging to this user
$sql = 'SELECT * FROM files where id=? AND (uid=? or public="y")';
$sth = $db->prepare ($sql);
$sth->execute (array ($_GET['id'], $user));
if ($row = $sth->fetch()) {	// If a record was found, return it
	header ('Content-type: '.$row['mime']);
	// Need to return filename.................
	echo $row['content'];
}
?>