<?php
session_start();					// Start session
require_once 'include/db.php';		// Connect to database
require_once 'classes/user.php';	// Do login stuff
if (!$user->isLoggedIn())			// No user is logged in
	die ("You do not have access to this page");

// Anything below this point means we have a valid user
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Protected page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css"/>
<link rel="stylesheet" href="signin.css"/>
</head>
<body>
	<div class="container">
		<h1>This content is protected</h1>
		<?php echo "Welcome {$user->getName()['givenname']} {$user->getName()['surename']}"; ?>
	</div>
</body>
</html>