<?php
session_start();					// Start the session
require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff

if ($user->isLoggedIn())
	header("Location: index.php");

if (isset($_POST['firstname'])) {	// Create new user
	$res = $user->addUser($_POST['email'], $_POST['passwd'], $_POST['firstname'], $_POST['lastname']);
	if (isset($res['success']))
		header("Location: index.php");
	else
		$newUserError = true;
}

$pageTitle = "Welcome to the Bookmarks Database";
require_once 'include/heading.php';

echo $user->insertLoginForm();
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<?php
// The following was added by Øivind Kolloen,
// You will learn about this during after the first project
if (isset ($newUserError)) { ?>
<script type="text/javascript">
$(function() {	// When document is loaded, hide the login and show signup
	$('#loginbox').hide(); $('#signupbox').show();
});
</script>
<?php 
}
?>
</body>
</html>