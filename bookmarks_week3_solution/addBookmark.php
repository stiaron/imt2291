<?php
session_start();					// Start the session

require_once 'include/db.php';		// Connect to the database
require_once 'classes/user.php';	// Do login stuff
require_once 'classes/categories.php';
require_once 'classes/bookmarks.php';	// Stores bookmarks in database

$pageTitle = "Add bookmark";
require_once 'include/heading.php';
?>

<div class="container">
<?php 
if ($user->isLoggedIn()) {
	echo '<div class="row"><div class="col-sm-4 col-xs-12">';
	echo '<div class="panel panel-default"><div class="panel-heading">Categories</div>';
	echo '<div class="panel-body">';
	$categories->insertCategoriesTree();
	echo '</div></div>';
	echo '</div><div class="col-sm-8 col-xs-12">';
	echo '<div class="panel panel-default"><div class="panel-heading">Add category</div>';
	echo '<div class="panel-body">';
	if (isset($_GET['urlToInvestgate']))	// URL is entered, show information about the page
		require_once 'include/addBookmark.inc.php';
	else if (isset($_GET['categoryID']))	// Get the URL to add as a bookmark
		require_once 'include/getURLToBookmarkForm.inc.php';
	echo '</div></div>';
	echo '</div>';
	
}
?>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="BootstrapTreeNav/dist/js/bootstrap-treenav.min.js"></script>
</body>
</html>