<?php 
/**
 * This script is used to display the list of bookmarks.
 * This script is included from classes/bookmarks.php
 * and when it is called there is an array with bookmarks
 * to display in the $bookmarkdata variable.
 */
foreach ($bookmarkdata as $bookmark) {	// Loop through all bookmarks ?>
<div class="row<?php
	if ($bookmark['id']==$this->justInsertedId)	// If this bookmark was just created
		echo " bg-success";		// Highlight it
	else 
		echo " alternate";		// if not, display every other row with light gray background
	?>">
	<div class="col-xs-2"><?php 
	if ($bookmark['thumbnailSize']>0)	// If we have a thumbnail image for this bookmark
		echo "<img style='margin-top:20px' class='img-responsive' src='dbImage.php?id={$bookmark['id']}'/>";	
	?></div>
	<div class="col-xs-9"><h3 style="margin-top: 5px; margin-bottom: 0px;">
		<a href="<?php echo $bookmark['url']; ?>"><?php echo $bookmark['title']?></a></h3>
	<p style="margin-bottom: 5px;"><?php echo $bookmark['description']?></p></div>
	<div class="col-xs-1">
	<span style="margin-top:10px" class="glyphicon glyphicon-pencil" aria-label="Edit"></span>
	<span class="glyphicon glyphicon-remove" aria-label="Remove"></span>
	</div>
</div>
<?php 
}
?>
<style type="text/css">
.alternate:nth-child(even) {
	background-color: #eee;
} 
</style>